import axios from 'axios/index'
import config from 'root/env-config'

function login (data) {
    return axios.post(`${config.apiUrl}/auth/login`, data)
        .then(response => {
            return response.data
        })
        .catch(error => {
            throw error
        })
}
function register (user) {
    return axios.post(`${config.apiUrl}/auth/register`, user)
        .then(response => {
            return response.data
        })
        .catch(error => {
            throw error
        })
}
function forgotPassword (data) {
    return axios.post(`${config.apiUrl}/auth/forgot`, data)
        .then(response => {
            return response.data
        })
        .catch(error => {
            throw error
        })
}
function logout () {
    return axios.post(`${config.apiUrl}/auth/logout`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            throw error
        })
}
function getUser () {
    return axios.get(`${config.apiUrl}/auth/profile`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            throw error
        })
}
function updateUser (data) {
    return axios.post(`${config.apiUrl}/auth/profile`, data)
        .then(response => {
            return response.data
        })
        .catch(error => {
            throw error
        })
}

export default {
    login,
    register,
    forgotPassword,
    logout,
    getUser,
    updateUser
}
