import store from '@/store'
import router from '@/router'

export function jwtInjector (config) {
    config.headers.Authorization = `Bearer ${store.getters.token}`
    return config
}

export function logoutOnUnauthorized (error) {
    if (error.response && error.response.status && error.response.status === 401) {
        store.dispatch('setToken', { token: '' })
        store.dispatch('setRole', { role: '' })
        store.dispatch('setUser', { user: null })
        localStorage.removeItem('lc-advertising')
        setTimeout(() => {
            router.push({ name: 'Welcome' })
        }, 300)
        return Promise.reject(error)
    } else {
        return Promise.reject(error)
    }
}
