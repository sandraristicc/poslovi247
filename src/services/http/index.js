import axios from 'axios'
import config from 'root/env-config'
import { jwtInjector, logoutOnUnauthorized } from './interceptors/'

axios.defaults.baseURL = config.apiUrl

axios.interceptors.request.use(jwtInjector, e => Promise.reject(e))
axios.interceptors.response.use(r => r, logoutOnUnauthorized)
