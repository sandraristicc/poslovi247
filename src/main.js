import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store'
import '@/services/http'

import VueRouter from 'vue-router'
import VModal from 'vue-js-modal'

Vue.use(VModal)


// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
// import 'bootstrap/dist/js/bootstrap.min.js'


Vue.use(VueRouter)


new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
