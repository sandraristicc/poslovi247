
import Welcome from "../components/welcome/Welcome";

export default [
  {
    path: '/',
    name: 'Welcome',
    component: Welcome
  },
  { path: '*', redirect: { name: 'Welcome' } }
]
