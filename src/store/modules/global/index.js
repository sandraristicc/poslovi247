import * as types from './types'
import api from '@/services/api/global'
import config from 'root/env-config'

const state = {
    token: '',
    user: null,
    role: ''
}
const getters = {
    token: state => state.token,
    user: state => state.user,
    role: state => state.role
}
const mutations = {
    [types.SET_TOKEN] (state, { token }) {
        state.token = token
        localStorage[config.local_storage_token] = token
    },
    [types.SET_ROLE] (state, { role }) {
        state.role = role
        localStorage[config.role] = role
    },
    [types.SET_USER] (state, { user }) {
        state.user = user
    }
}
const actions = {
    setToken ({ commit }, { token }) {
        commit(types.SET_TOKEN, { token })
    },
    setUser ({ commit }, { user }) {
        commit(types.SET_USER, { user })
    },
    setRole ({ commit }, { role }) {
        commit(types.SET_ROLE, { role })
    },
    login ({ commit, dispatch }, data ) {
        return api.login(data)
            .then(response => {
                commit(types.SET_TOKEN, { token: response.data.access_token })
                return dispatch('getUser')
            })
            .catch(error => {
                throw error
            })
    },
    register (store, user) {
        return api.register(user)
            .then(response => {
                return response
            })
            .catch(error => {
                throw error
            })
    },
    getUser ({ commit }) {
        return api.getUser()
            .then(response => {
                commit(types.SET_USER, { user: response.data })
                commit(types.SET_ROLE, { role: response.data.roles[0].name })
                return response
            })
            .catch(error =>{
                throw error
            })
    },
    logout ({ dispatch }) {
        return api.logout()
            .then(data => {
                dispatch('setToken', { token: '' })
                dispatch('setRole', { role: '' })
                dispatch('setUser', { user: null })
                localStorage.removeItem('jd-client')
                return data
            })
            .catch(error => error)
    },
}
export default { state, getters, mutations, actions }
