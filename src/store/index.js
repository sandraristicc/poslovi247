import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import globalModule from '@/store/modules/global'



Vue.use(Vuex)

const store = new Vuex.Store({
    modules: { globalModule},
    state: {
    },
    mutations: {
    },
    actions: {
    },
    getters: {
    },
    plugins: [createPersistedState({
        key: 'poslovi247',
        reducer (value) {
            if(value.globalModule.token === '') {
                return {}
            }
            return value
        } })]
})

export default store
